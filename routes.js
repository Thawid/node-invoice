const medicineController = require('./controller/medicineController');
const customerController = require('./controller/customerController');
const representativeController = require('./controller/representativeController');
const sellController = require('./controller/sellController');

const registerRoutes = (app)=>{
  app.use('/product',medicineController);
  app.use('/customer',customerController);
  app.use('/representative',representativeController);
  app.use('/sell',sellController);
}



module.exports = {
  registerRoutes,
}