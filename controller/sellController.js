const express = require('express');
const router = express.Router();
const sellModel = require('../model/sellModel');
const { validateRequest } = require('../validator/sellValidator');
const Response = require('../utils/response');
const logger = require('../utils/logger')

router.post('/',async (req,res) =>{
        const response = new Response(res);

        try {
            logger.info('SellController::Add');
            const { data, error } = validateRequest(req,'add');
            if(error) return response.badRequest(error);
            const { customer_id,representative_id,invoice_no,total_qty,total_bonus,total_product_price,product_id,product_qty,product_bonus,product_price,sell_id } = data;
            await sellModel.add({ customer_id,representative_id,invoice_no,total_qty,total_bonus,total_product_price,product_id,product_qty,product_bonus,product_price,sell_id });
            const payload = {
                status : true,
                message : "Sell completed successfully"
            }
            return response.content(payload);
        }catch (error){
            logger.error(`SellController::Add`);
            const message = error.message ? error.message : 'Server error';
            response.internalServerError({ message });
        }
});




module.exports = router;