const express = require('express');
const router = express.Router();
const representativeController = require('../model/representativeModel');
const { validateRequest } = require('../validator/representativeValidator');
const Response = require('../utils/response');
const logger = require('../utils/logger')

// require your models here

router.get('/',async (req,res) =>{

    //return res.json({ message : 'Get All Representative'});

    logger.info('RepresentativeController::Get All Representative');
    const response = new Response(res);
    try {
        const customers = await representativeController.getAllRepresentative();
        return response.ok(customers);
    }catch (error){
        logger.error('RepresentativeController::Get All Representative');
        const message = error.message ? error.message : 'Server error';
        response.internalServerError( { message });
    }
});


router.get('/:representativeId',async (req,res) =>{
    const response = new Response(res);
    try {
        logger.info('RepresentativeController::Get Representative');
        const id = req.params.representativeId;

        const customer = await representativeController.get(id);
        if(!customer) return response.notFound({message :  'Representative not found'});
        return response.ok(customer);

    }catch (error){
        logger.error(`RepresentativeController::Get Representative`);
        const message = error.message ? error.message : 'Server error';
        response.internalServerError({ message });
    }
});

router.post('/', async (req,res) =>{
    const response = new Response(res);
    try {
        logger.info('RepresentativeController::Add Representative');
        const { data , error } = validateRequest(req, 'add');
        if(error) return  response.badRequest(error);
        //const { name,packing,size,price } = req.body;
        const { name,phone,zone } = data;
        const customer = await representativeController.add({ name,phone,zone });
        const payload = {
            status : true,
            message : "Representative add successfully"
        }
        return response.content(payload);
    }catch (error){
        logger.error(`RepresentativeController::Add Representative`);
        const message = error.message ? error.message : 'Server error';
        response.internalServerError({ message });
    }
});


router.put('/:representativeId', async (req, res) =>{
    const response = new Response(res);
    try {
        logger.error(`RepresentativeController::Update Representative`);
        const { data, error } = validateRequest(req, 'update');
        if(error) return response.badRequest(error);
        //const {name, packing, size, price } = data;
        const id = req.params.representativeId;
        const customer = await representativeController.get(id);
        console.log(customer)
        if(!customer) return response.notFound({ message :  'Representative not found'});

        const payload = {...data}
        const result = await representativeController.update(payload,id);
        success = {
            status : true,
            message : "Representative update successfully",
        }
        return response.content(success);
    }catch (error){
        logger.error(`RepresentativeController::Update Representative`);
        const message = error.message ? error.message : 'Server error';
        response.internalServerError({ message });
    }
});

router.delete('/:representativeId', async (req,res)=>{
    const response = new Response(res);
    try {
        const id = req.params.representativeId;
        const product = await representativeController.get(id);
        console.log(product);
        if(!product) return response.notFound({ message :  'Customer not found'});
        await representativeController.remove(id);
        const payload = {
            status : true,
            message : 'Representative delete successfully'
        }
        return response.content(payload);
    }catch (error){
        logger.error(`RepresentativeController::Remove Representative`);
        const message = error.message ? error.message : 'Server error';
        response.internalServerError({ message });
    }
});

module.exports = router;