const express = require('express');
const router = express.Router();
const customerModel = require('../model/customerModel');
const { validateRequest } = require('../validator/customerValidator');
const Response = require('../utils/response');
const logger = require('../utils/logger')

// require your models here

router.get('/',async (req,res) =>{

    //return res.json({ message : 'Get all customer'});

    logger.info('CustomerController::Get All Customer');
    const response = new Response(res);
    try {
        const customers = await customerModel.getAllCustomers();
        return response.ok(customers);
    }catch (error){
        logger.error('CustomerController::Get All Customer');
        const message = error.message ? error.message : 'Server error';
        response.internalServerError( { message });
    }
});


router.get('/:customerId',async (req,res) =>{
    const response = new Response(res);
    try {
        logger.info('CustomerController::Get Customer');
        const id = req.params.customerId;

        const customer = await customerModel.get(id);
        if(!customer) return response.notFound({message :  'Customer not found'});
        return response.ok(customer);

    }catch (error){
        logger.error(`CustomerController::Get Customer`);
        const message = error.message ? error.message : 'Server error';
        response.internalServerError({ message });
    }
});

router.post('/', async (req,res) =>{
    const response = new Response(res);
    try {
        logger.info('CustomerController::Add Customer');
        const { data , error } = validateRequest(req, 'add');
        if(error) return  response.badRequest(error);
        //const { name,packing,size,price } = req.body;
        const { customer_name,phone,address } = data;
        const customer = await customerModel.add({ customer_name,phone,address });
        const payload = {
            status : true,
            message : "Customer add successfully"
        }
        return response.content(payload);
    }catch (error){
        logger.error(`CustomerController::Add Customer`);
        const message = error.message ? error.message : 'Server error';
        response.internalServerError({ message });
    }
});


router.put('/:customerId', async (req, res) =>{
    const response = new Response(res);
    try {
        logger.error(`CustomerController::Update Customer`);
        const { data, error } = validateRequest(req, 'update');
        if(error) return response.badRequest(error);
        //const {name, packing, size, price } = data;
        const id = req.params.customerId;
        const customer = await customerModel.get(id);
        console.log(customer)
        if(!customer) return response.notFound({ message :  'Customer not found'});

        const payload = {...data}
        const result = await customerModel.update(payload,id);
        success = {
            status : true,
            message : "Customer update successfully",
        }
        return response.content(success);
    }catch (error){
        logger.error(`CustomerController::Update Customer`);
        const message = error.message ? error.message : 'Server error';
        response.internalServerError({ message });
    }
});

router.delete('/:customerId', async (req,res)=>{
    const response = new Response(res);
    try {
        const id = req.params.customerId;
        const product = await customerModel.get(id);
        console.log(product);
        if(!product) return response.notFound({ message :  'Customer not found'});
        await customerModel.remove(id);
        const payload = {
            status : true,
            message : 'Customer delete successfully'
        }
        return response.content(payload);
    }catch (error){
        logger.error(`CustomerController::Remove Customer`);
        const message = error.message ? error.message : 'Server error';
        response.internalServerError({ message });
    }
});

module.exports = router;