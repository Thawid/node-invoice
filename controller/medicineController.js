const express = require('express');
const router = express.Router();
const productModel = require('../model/productModel');
const { validateRequest } = require('../validator/productValidator');
const Response = require('../utils/response');
const logger = require('../utils/logger')

// require your models here

router.get('/',async (req,res) =>{

    //return res.json({ message : 'Get all product'});
    logger.info('MedicineController::Get All Product');
    const response = new Response(res);
    try {
        const products = await productModel.getAllProducts();
        return response.ok(products);
    }catch (error){
        logger.error('MedicineController::Get All Product');
        const message = error.message ? error.message : 'Server error';
        response.internalServerError( { message });
    }
});


router.get('/:productId',async (req,res) =>{
    const response = new Response(res);
    try {
        logger.info('MedicineController::Get Product');
        const id = req.params.productId;

        const product = await productModel.get(id);
        if(!product) return response.notFound({message : 'Product not found'});
        return response.ok(product);

    }catch (error){
        logger.error(`MedicineController::Get Product`);
        const message = error.message ? error.message : 'Server error';
        response.internalServerError({ message });
    }
});

router.post('/', async (req,res) =>{
   const response = new Response(res);
   try {
       logger.info('MedicineController::Add Product');
       const { data , error } = validateRequest(req, 'add');
       if(error) return  response.badRequest(error);
       //const { name,packing,size,price } = req.body;
       const { name,packing,size,price } = data;
       const product = await productModel.add({ name,packing,size,price });
       const payload = {
           status : true,
           message : "Product add successfully"
       }
       return response.content(payload);
   }catch (error){
       logger.error(`MedicineController::Add Product`);
       const message = error.message ? error.message : 'Server error';
       response.internalServerError({ message });
   }
});


router.put('/:productId', async (req, res) =>{
    const response = new Response(res);
    try {
        logger.error(`MedicineController::Update Product`);
        const { data, error } = validateRequest(req, 'update');
        if(error) return response.badRequest(error);
        //const {name, packing, size, price } = data;
        const id = req.params.productId;
        const product = await productModel.get(id);
        console.log(product)
        if(!product) return response.notFound({ message : 'Product not found'});

        const payload = {...data}
        const result = await productModel.update(payload,id);
        success = {
            status : true,
            message : "Product update successfully",
        }
        return response.content(success);
    }catch (error){
        logger.error(`MedicineController::Update Product`);
        const message = error.message ? error.message : 'Server error';
        response.internalServerError({ message });
    }
});

router.delete('/:productId', async (req,res)=>{
   const response = new Response(res);
   try {
       const id = req.params.productId;
       const product = await productModel.get(id);
       console.log(product);
       if(!product) return response.notFound({ message : 'Product not found'});
       await productModel.remove(id);
       const payload = {
           status : true,
           message : 'Product delete successfully'
       }
       return response.content(payload);
   }catch (error){
       logger.error(`MedicineController::Remove Product`);
       const message = error.message ? error.message : 'Server error';
       response.internalServerError({ message });
   }
});

module.exports = router;