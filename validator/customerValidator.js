const { validate } = require('../utils/request-validator');

const rules = {
    add : {
        customer_name:{ type:'string', unique: true, empty : false, min:3, max:30 },
        phone : { type: 'string', empty: false, unique: true, min:11, max:15},
        address : { type : 'string', empty: false, min:3, max: 50},
    },
    update : {
        customer_name:{ type:'string', min:3, max:30, optional:true },
        phone : { type: 'string', optional: true},
        address : { type : 'string',  min:3, max: 50, optional: true},
    }


};

const validateRequest = (req, ruleName) => {
    const rule = rules[ruleName];
    return validate(req, rule);
};

module.exports = {
    validateRequest,
};