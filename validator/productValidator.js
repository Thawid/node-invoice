const { validate } = require('../utils/request-validator');

const rules = {
  add : {
          name:{ type:'string', unique: true, empty : false, min:3, max:30 },
          packing : { type: 'number', empty: false, integer : true, positive : true},
          size : { type : 'string', empty: false, min:3, max: 20},
          price : { type:'number', empty: false, integer: true, positive: true }
  },
  update : {
      name:{ type:'string', min:3, max:30, optional:true },
      packing : { type: 'number',  integer : true, positive : true, optional: true},
      size : { type : 'string',  min:3, max: 20, optional: true},
      price : { type:'number', integer: true, positive: true, optional:true }
  }


};

const validateRequest = (req, ruleName) => {
  const rule = rules[ruleName];
  return validate(req, rule);
};

module.exports = {
  validateRequest,
};