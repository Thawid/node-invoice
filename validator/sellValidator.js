const { validate } = require('../utils/request-validator');

const rules = {
    add : {
        customer_id:{ type:'number', empty : false, integer:true },
        representative_id:{ type:'number', empty : false, integer:true },
        invoice_no:{ type:'string', empty : false},
        total_qty:{ type:'number', empty : false, integer:true, positive:true },
        //total_bonus:{ type:'number', empty : false, integer:true, positive:true },
        total_bonus:"number|integer",
        total_product_price:{ type:'number', empty : false, integer:true, positive:true },
        product_id:{ type:'array', empty : false, integer:true, positive:true },
        product_qty:{ type:'array', empty : false, integer:true, positive:true },
        product_bonus:{ type:'array', empty : false, integer:true, positive:true },
        product_price:{ type:'array', empty : false, integer:true, positive:true },
        //sell_id:{ type:'array', empty : false, integer:true, positive:true },
    },
    update : {
        customer_id:{ type:'number', empty : false, integer:true },
        representative_id:{ type:'number', empty : false, integer:true },
        invoice_no:{ type:'string', empty : false},
        total_qty:{ type:'number', empty : false, integer:true, positive:true },
        total_bonus:{ type:'number', empty : false, integer:true, positive:true },
        total_product_price:{ type:'number', empty : false, integer:true, positive:true },
        product_id:{ type:'array', empty : false, integer:true, positive:true },
        product_qty:{ type:'array', empty : false, integer:true, positive:true },
        product_bonus:{ type:'array', empty : false, integer:true, positive:true },
        product_price:{ type:'array', empty : false, integer:true, positive:true },
        //sell_id:{ type:'array', empty : false, integer:true, positive:true },
    }


};

const validateRequest = (req, ruleName) => {
    const rule = rules[ruleName];
    return validate(req, rule);
};

module.exports = {
    validateRequest,
};