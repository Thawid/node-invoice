const db = require('../db');
const moment = require('moment');
const logger = require('../utils/logger');
const tableName = 'representatives';
const idColumn = 'id';



const getAllRepresentative = async ()=>{
    logger.info('Representative Model:: getAllRepresentative()');
    const sql = `SELECT * FROM ${tableName} ORDER BY ${idColumn} DESC`;
    const result = await db.query(sql);
    return result;
}

const get = async (id) =>{
    logger.info('Representative Model::get()');
    const sql = `SELECT * FROM ${tableName} WHERE ${idColumn} = ${db.escape(id)}`;
    const customer = await db.query(sql);
    return customer.length > 0 ? customer[0] : null;
}

const add = async ({name,phone,zone}) =>{
    logger.info('Representative Model::add()');
    const now = moment().format('YYYY-MM-DD HH:mm:ss');
    const  sql = `INSERT INTO ${tableName} VALUES (null,?,?,?,?,?)`;
    return await db.query(sql,[name,phone,zone,now,now]);
}

const update = async (data, id) => {
    logger.info('Representative Model::update()');
    const now = moment().format('YYYY-MM-DD HH:mm:ss');
    const sql = `UPDATE ${tableName} SET ? WHERE ${idColumn} = ${db.escape(id)}`;
    return await db.query(sql,data);
}

const remove = async (id)=>{
    logger.info('Representative Model::remove()');
    const sql = `DELETE FROM ${tableName} WHERE ${idColumn} = ${db.escape(id)}`;
    return await db.query(sql);
}



module.exports = {
    getAllRepresentative,
    get,
    add,
    update,
    remove
}