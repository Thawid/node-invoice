const db = require('../db');
const moment = require('moment');
const logger = require('../utils/logger');

const add = async ({ customer_id,representative_id,invoice_no,total_qty,total_bonus,total_product_price,product_id,product_qty,product_bonus,product_price,sell_id })=>{
    logger.info('Sell Model::add()');
    const now = moment().format('YYYY-MM-DD HH:mm:ss');
    await db.beginTransaction();
    try {
        let sql = `INSERT INTO sells VALUES (null,?,?,?,?,?,?,?,?)`;
        const sell = await db.query(sql,[customer_id,representative_id,invoice_no,total_qty,total_bonus,total_product_price,now,now]);
        const sellId = sell.insertId;
        for (let i=0; i< product_id.length; i++){
            sql = `INSERT INTO invoices VALUES (null,?,?,?,?,?,?,?,?,?,?)`;
            await db.query(sql,[product_id[i],product_qty[i],product_bonus[i],product_price[i],customer_id,representative_id,sellId,1,now,now]);
        }
        await db.commit();
    }catch (error){
        logger.error('Sell Model::add()');
        console.log(error)
        db.rollback();
        //return throw error;
    }


};



module.exports = {
    add
}