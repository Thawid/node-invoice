const db = require('../db');
const moment = require('moment');
const logger = require('../utils/logger');
const tableName = 'medicines';
const idColumn = 'id';



const getAllProducts = async ()=>{
    logger.info('Product Model:: getAllProducts()');
    const sql = `SELECT * FROM ${tableName} ORDER BY ${idColumn} DESC`;
    const result = await db.query(sql);
    return result;
}

const get = async (id) =>{
    logger.info('Product Model::get()');
    const sql = `SELECT * FROM ${tableName} WHERE ${idColumn} = ${db.escape(id)}`;
    const product = await db.query(sql);
    return product.length > 0 ? product[0] : null;
}

const add = async ({name,packing,size,price}) =>{
    logger.info('Product Model::add()');
    const now = moment().format('YYYY-MM-DD HH:mm:ss');
    const  sql = `INSERT INTO ${tableName} VALUES (null,?,?,?,?,?,?)`;
    return await db.query(sql,[name,packing,size,price,now,now]);
}

const update = async (data, id) => {
    logger.info('Product Model::update()');
    const now = moment().format('YYYY-MM-DD HH:mm:ss');
    const sql = `UPDATE ${tableName} SET ? WHERE ${idColumn} = ${db.escape(id)}`;
    return await db.query(sql,data);
}

const remove = async (id)=>{
    logger.info('Product Model::remove()');
    const sql = `DELETE FROM ${tableName} WHERE ${idColumn} = ${db.escape(id)}`;
    return await db.query(sql);
}



module.exports = {
    getAllProducts,
    get,
    add,
    update,
    remove
}